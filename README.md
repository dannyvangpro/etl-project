## ETL Mélanie LECLEZIAT - Danny VANG

Dashboard

## Pré-requie
VueJS - JavaScript / Django - Python / ElasticSearch

## Concept Appliqué 
- Visualiation de donnée d'un JSON 

## Cloner le projet 

```bash
git clone https://gitlab.com/dannyvangpro/etl-project.git
```

## Installer les dépendances du front + Lancement 

```bash
cd front 
npm i 

npm run dev
```

## Installer les dépendances du back + Lancement 

```bash
cd backetlback
python3 manage.py runserver
```

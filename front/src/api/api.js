import axios from 'axios';

const apiUrl = 'http://127.0.0.1:8000/';

export const getSale = async (id) => {
  const url = `${apiUrl}api/getProduit/${id}`;
  try {
    const response = await axios.get(url);
    console.log("api", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getCategorie = async (cat_id) => {
  const url = `${apiUrl}api/getCategorie/${cat_id}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    throw error; 
  }
};


export const getProductbyCategory = async () => {
  const url = `${apiUrl}api/getAllCategorieProduit`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getAllProduitVente = async () => {
  const url = `${apiUrl}api/getAllProduitVente`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response : ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getNbMagasinVente = async () => {
  const url = `${apiUrl}api/getNbMagasinVente`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response magasin: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getByDate = async (start, end, idMag) => {
  const url = `${apiUrl}api/getByDate/${start}/${end}/${idMag}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response magasin: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getDate = async (start) => {
  const url = `${apiUrl}api/getDate/${start}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api réponse getDate: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getProduitVente = async (idProd) => {
  const url = `${apiUrl}api/getProduitVente/${idProd}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response magasin: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getFab = async (idFab) => {
  const url = `${apiUrl}api/getFab/${idFab}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response fabricant: ", response.data)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const compareVenteProduit = async (idFab) => {
  const url = `${apiUrl}api/compareVenteProduit`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response magasin: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getNbCategorieVente = async () => {
  const url = `${apiUrl}api/getNbCategorieVente`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response magasin: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getNbAllMagasin = async (id) => {
  const url = `${apiUrl}api/getNbAllMagasin/${id}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response magasin: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getAllFab = async () => {
  const url = `${apiUrl}api/getAllFab`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response magasin: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const GetMagidCounts = async () => {
  const url = `${apiUrl}api/GetMagidCounts`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response magasin: ", response)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const SalesByMagasin = async (id) => {
  const url = `${apiUrl}api/salesByMagasin/${id}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response nb vente magasin: ", response.data)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const GetTopNbVenteByProd = async () => {
  const url = `${apiUrl}api/GetTopNbVenteByProd`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response top 10 ", response.data)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getAllProductByCatAndByFabId = async (id) => {
  const url = `${apiUrl}api/getAllProductByCatAndByFabId/${id}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response top 10 ", response.data)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getProdByIdByMonth = async (id) => {
  const url = `${apiUrl}api/getProdByIdByMonth/${id}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response prod by month", response.data)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const getSaleByCatAndFabId = async (id) => {
  const url = `${apiUrl}api/getSaleByCatAndFabId/${id}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response prod by cat", response.data)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const top10ProdByCatAndFab = async (id) => {
  const url = `${apiUrl}api/top10ProdByCatAndFab/${id}`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response prod by cat", response.data)
    return response.data;
  } catch (error) {
    throw error; 
  }
};

export const top10Prod = async () => {
  const url = `${apiUrl}api/top10Prod/`; 
  console.log(url);
  try {
    const response = await axios.get(url);
    console.log("dossier api response prod by cat", response.data)
    return response.data;
  } catch (error) {
    throw error; 
  }
};


export default{
  getSale,
  getCategorie,
  getProductbyCategory,
  getAllProduitVente,
  getNbMagasinVente,
  getByDate,
  getDate,
  getProduitVente,
  getFab,
  compareVenteProduit,
  getNbCategorieVente,
  getNbAllMagasin,
  getAllFab,
  GetMagidCounts,
  GetTopNbVenteByProd,
  getAllProductByCatAndByFabId,
  getProdByIdByMonth,
  getSaleByCatAndFabId,
  top10Prod
};

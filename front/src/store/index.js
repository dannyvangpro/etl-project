// store.js
import Vue from 'vue';
import Vuex from 'vuex';
import { getSale } from "@/api/api";
import { getCategorie } from "@/api/api";
import { createStore } from 'vuex';


const store = createStore({
    state: {
        saleList:[],
        sales:[],
        test: 'Bloublou'
    },
    getters:{
        getTest(state){
            return state.test;
        }
      },
    mutations: {
        SET_SALES_BY_PRODUCT:(state, products) => {
            state.saleList = products
          },

        SET_SALES_BY_CATEGORY:(state, sales)=>{
        state.saleList = sales
        }
      
    },
    actions: {
        async fetchsale({commit, state}, id) {
            try {
              const response = await getSale(id);
              this.sale = response;
              console.log("sale by product", response)
              commit('SET_SALES_BY_PRODUCT', response)
            } catch (error) {
              console.error("Failed to fetch sale:", error);
            }
        },

        async fetchCat({commit, state}, cat_id) {
            try {
                const response = await getCategorie(cat_id);
                commit('SET_SALES_BY_CATEGORY', response)
                console.log("sale by categorie", response)
            }catch (error) {
                console.error("Failed to fetch category:", error);
            }
        }  
    }
})


export default store;

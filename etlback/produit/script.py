import gzip,shutil
from django.conf import settings
import os 
from elasticsearch import Elasticsearch


#es = Elasticsearch( hosts=["https://localhost:9200"],http_auth=('myuser', 'password'),verify_certs=False)
index_name = "produits"

def getCat(id):
    list = []
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
                "query": {
                    "term": {
                        "cat_id": id
                    }
                },
                "size": 1000  
            }
        )
    for e in val["hits"]["hits"] :
        list.append(e["_source"])
    return list
     

def getDate(date):
    list=[]
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
                "query": {
                    "term": {
                        "date":date
                    }
                },
                "size": 1000  
            }
        )
    for e in val["hits"]["hits"] :
        list.append(e["_source"])
    return list
     

def getProduit(prod):
    list=[]
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
                "query": {
                    "term": {
                        "prod_id":prod
                    }
                },
                "size": 1000  
            }
        )
    return val["hits"]["hits"][0]["_source"]


def getFab(FABID):
    result_list = []
    
    val = settings.ELASTICSEARCH_CLIENT.search(
        index=index_name,
        body={
            "query": {
                "term": {
                    "fab_id": FABID
                }
            },
            "size": 1000
        }
    )
    
    for hit in val["hits"]["hits"]:
        result_list.append(hit["_source"])
    
    return result_list


def eachCategorie(): 
    d = {}
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
                "size": 10000,
                "aggs": {
                    "fab_counts": {
                        "terms": {
                            "field": "cat_id",
                            "size": 100000  # Mettez une valeur suffisamment élevée pour capturer tous les résultats
                        }
                    }
                }
            }
        )
    for e in val["hits"]["hits"] :
        key=e["_source"]["cat_id"]
        if key in d:
            d[key] += 1
        else:
           d[key] = 1
    
    return d





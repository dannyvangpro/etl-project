from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import generics,status
from rest_framework.response import Response
from .script import *
from django.http import JsonResponse

class GetCategorie(APIView):
    def get(self,request,pk,format=None):
        if request.method == "GET":
            #value = getCat(pk)
            #return JsonResponse({'response' : value})
            val = getCat(pk)
            return JsonResponse(val,safe=False)
        return Response(status=404)
    
class GetDate(APIView):
    def get(self,request,date,format=None):
        if request.method == "GET":
            #value = getCat(pk)
            #return JsonResponse({'response' : value})
            val = getDate(date)
            return JsonResponse(val,safe=False)
        return Response(status=404)
    

class GetProduit(APIView):
    def get(self,request,pk,format=None):
        if request.method == "GET":
            #value = getCat(pk)
            #return JsonResponse({'response' : value})
            val = getProduit(pk)
            print(val)
            return JsonResponse(val,safe=False)
        return Response(status=404)
    
class GetFab(APIView):
    def get(self, request, pk, format=None):
        if request.method == "GET":
            print("id fab", pk)
            products = getFab(pk)  
            print("Products par fabricant", getFab(pk))
            return JsonResponse(products, safe=False)
        return Response(status=404)

class ChaqueCategorie(APIView):
    def get(self,request,format=None):
        if request.method == "GET":
            #value = getCat(pk)
            #return JsonResponse({'response' : value})
            val = eachCategorie()
            return JsonResponse(val,safe=False)
        return Response(status=404)

from django.urls import path,include

from produit import views

urlpatterns = [
    #retourne un dictionnaire avec le nombre d'occurence pour chaque catégorie
    path("api/getAllCategorieProduit", views.ChaqueCategorie.as_view()),

    #entree: la numero de la categorie / sortie: la liste des produits ayant la categorie
    path("api/getCategorie/<int:pk>", views.GetCategorie.as_view()),

    #entree: une date / sortie : une liste de produits ayant la date 
    path("api/getDate/<str:date>", views.GetDate.as_view()),

    #entree : un produit / sortie : retourne le produit
    path("api/getProduit/<int:pk>", views.GetProduit.as_view()),

    #entree: le fabID / sortie: retourne le produit avec le fabID
    path("api/getFab/<int:pk>", views.GetFab.as_view()),

    # #entrée : fabId - sortie liste de produit par la fabricant
    # path("api/getProductByFabId",views.getProductByFabId.as_view()),

    # #entrée : fabId - sortie liste de des ventes par la fabricant
    # path("api/getSaleByFabId",views.getSaleByFabId.as_view()),

    
]
import json
from datetime import datetime
from your_app.models import DataEntry

# Lire le JSON depuis un fichier
with open('data.json', 'r') as file:
    data = json.load(file)

# Parcourir les entrées et les sauvegarder dans la base de données
for entry in data:
    date_object = datetime.strptime(entry["DATE"], '%Y%m%d').date()
    DataEntry(
        date=date_object,
        prodid=entry["PRODID"],
        catid=entry["CATID"],
        fabid=entry["FABID"]
    ).save()
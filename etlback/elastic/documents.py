from django_elasticsearch_dsl import Document, Index
from .models import Product

data_entries = Index('data_entries')

@data_entries.document
class DataEntryDocument(Document):
    class Django:
        model = Product
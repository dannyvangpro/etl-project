from django.db import models

class Product(models.Model):
    date = models.DateField()
    prod_id = models.IntegerField()
    cat_id = models.IntegerField()
    fab_id = models.IntegerField()

class Vente(models.Model):
    date = models.DateField()
    prod_id = models.IntegerField()
    cat_id = models.IntegerField()
    fab_id = models.IntegerField()
    mag_id = models.IntegerField()
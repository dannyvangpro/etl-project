from elasticsearch import Elasticsearch, helpers
from django.core.management.base import BaseCommand
import time
import json
from datetime import datetime

class Command(BaseCommand):
    help = 'Index JSON data into Elasticsearch'

    def handle(self, *args, **kwargs):
        start_time = time.time()

        es = Elasticsearch(
            hosts=["https://localhost:9200"],
            http_auth=('myuser', 'password'),
            verify_certs=False
        )

        # Vérifiez si l'index existe
        if not es.indices.exists(index="vente"):
            # Créez l'index s'il n'existe pas
            es.indices.create(index="vente")

        # Désactivez les rafraîchissements
        es.indices.put_settings(index="vente", body={"refresh_interval": "-1"})

        # Lire le fichier et indexe les données
        def generate_actions():
            with open('elastic/pointsDeVente-tous.json', 'r') as file:
                data_list = json.load(file)  # Chargez le tableau JSON complet
                for data in data_list:
                    date_obj = datetime.strptime(data['DATE'], '%Y%m%d').date()
                    yield {
                        "_index": "vente",
                        "_op_type": "index",
                        "_source": {
                            "date": date_obj,
                            "prod_id": data['PRODID'],
                            "cat_id": data['CATID'],
                            "fab_id": data['FABID'],
                            "mag_id": data['MAGID']
                        }
                    }

        # Indexer en utilisant l'indexation parallèle
        for _ in helpers.parallel_bulk(es, generate_actions(), thread_count=4, chunk_size=1000):
            pass

        # Réactivez les rafraîchissements
        es.indices.put_settings(index="vente", body={"refresh_interval": "1s"})

        end_time = time.time()
        execution_time = end_time - start_time

        self.stdout.write(self.style.SUCCESS(f'Successfully indexed JSON data in {execution_time:.2f} seconds'))

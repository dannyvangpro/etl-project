from elasticsearch import Elasticsearch
from django.core.management.base import BaseCommand
import ssl

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        # Connecter au cluster Elasticsearch
        es = Elasticsearch(hosts="http://localhost:9200", basic_auth=("myuser", "password"))


        # Vérifier si l'index existe
        index_name = "produits"  # remplacer par le nom de votre index

        # Exécuter une requête pour obtenir toutes les ventes avec cat_id = 4
        response = es.search(
            index=index_name,
            body={
                "query": {
                    "term": {
                        "cat_id": 3
                    }
                },
                "size": 1000  # Récupérer 1000 documents. Augmentez si vous avez plus de documents.
            }
        )

        # Afficher les résultats
        for doc in response['hits']['hits']:
            print(doc['_source'])

        # Si vous avez plus de 1000 documents (ou le nombre que vous avez défini avec "size"), 
        # vous devrez peut-être utiliser la pagination ou le scroll pour récupérer tous les documents.

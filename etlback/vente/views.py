from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import generics,status
from rest_framework.response import Response
from .script import *
from django.http import JsonResponse

class DateMagasin(APIView):
    def get(self,request,date,pk,format=None):
        if request.method == "GET":
            val = getDateMag(date,pk)
            return JsonResponse(val,safe=False)
        return Response(status=404)
    
class DateCategorie(APIView):
    def get(self,request,date,pk,format=None):
        if request.method == "GET":
            val = getDateCat(date,pk)
            return JsonResponse(val,safe=False)
        return Response(status=404)

class Magasin(APIView):
    def get(self,request,pk,format=None):
        if request.method == "GET":
            val = getMagasin(pk)
            reponse= {"nbMagasin":val}
            return JsonResponse(reponse,safe=False)
        return Response(status=404)
    
class ChaqueMagasin(APIView):
    def get(self,request,format=None):
        if request.method == "GET":
            val = eachMagasin()
            return JsonResponse(val,safe=False)
        return Response(status=404)
    

class ChaqueCategorie(APIView):
    def get(self,request,format=None):
        if request.method == "GET":
            val = eachCategorie()
            return JsonResponse(val,safe=False)
        return Response(status=404)

class ByDate(APIView):
    def get(self,request,debut,fin,magid,format=None):
        if request.method == "GET":
            val = intervalleDate(debut,fin,magid)
            return JsonResponse(val,safe=False)
        return Response(status=404)
    
class Compare(APIView):
    def get(self,request,format=None):
        if request.method == "GET":
            val = compareVenteProduit()
            response = {"Nombre de produits hors vente",val}
            return JsonResponse(val,safe=False)
        return Response(status=404)

class GetProduit(APIView):
    def get(self,request,pk,format=None):
        if request.method == "GET":
            val = getProduit(pk)
            return JsonResponse(val,safe=False)
        return Response(status=404)
    
class GetAllProduit(APIView):
    def get(self,request,format=None):
        if request.method == "GET":
            val = produits_par_mois()
            return JsonResponse(val,safe=False)
        return Response(status=404)
    

class GetAllFab(APIView):
    def get(self,request,format=None):
        if request.method == "GET":
            val = getAllFab()
            return JsonResponse(val,safe=False)
        return Response(status=404)
    
#compte le nb vente par maqgasin 
class GetMagidCounts(APIView):
    def get(self, request, format=None):
        if request.method == "GET":
            magid_counts = getMagidCounts() 
            return JsonResponse(magid_counts, safe=False)
        return Response(status=404)
    

class SalesByMagasin(APIView):
    def get(self, request, pk,format=None):
        if request.method == "GET":
            magasin_sales = produits_par_mois_par_mag(pk) 

            return JsonResponse(magasin_sales, safe=False)
        return JsonResponse({"error": "Invalid request method"}, status=405)
    
class GetTopNbVenteByProd(APIView):
    def get(self, request, format=None):
        if request.method == "GET":
            top10 = getTop10ProduitsLesPlusVendus() 
            return JsonResponse(top10, safe=False)
        return Response(status=404)


class getAllProductByCatAndByFabId(APIView):
    def get(self, request, pk,format=None):
        if request.method == "GET":
            saleByCat = ventes_par_categorie(pk) 

            return JsonResponse(saleByCat, safe=False)
        return JsonResponse({"error": "Invalid request method"}, status=405)
    

class getProdByIdByMonth(APIView):
    def get(self, request, pk,format=None):
        if request.method == "GET":
            saleByCat = produits_par_mois_parIdProd(pk) 

            return JsonResponse(saleByCat, safe=False)
        return JsonResponse({"error": "Invalid request method"}, status=405)
    
class getSaleByCatAndFabId(APIView):
    def get(self, request, pk,format=None):
        if request.method == "GET":
            saleByCatAndFabId = ventes_par_categorie_et_fabid(pk) 

            return JsonResponse(saleByCatAndFabId, safe=False)
        return JsonResponse({"error": "Invalid request method"}, status=405)
    
class top10ProdByCatAndFab(APIView):
    def get(self, request, pk,format=None):
        if request.method == "GET":
            top10saleByCatAndFabId = ventes_par_fabid(pk) 

            return JsonResponse(top10saleByCatAndFabId, safe=False)
        return JsonResponse({"error": "Invalid request method"}, status=405)
    

class top10Prod(APIView):
    def get(self, request,format=None):
        if request.method == "GET":
            top10ProdSale = getTop10ProduitsLesPlusVendus() 

            return JsonResponse(top10ProdSale, safe=False)
        return JsonResponse({"error": "Invalid request method"}, status=405)
    

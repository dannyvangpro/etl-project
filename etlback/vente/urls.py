from django.urls import path,include

from vente import views

urlpatterns = [

    #retourne le nombre d'occurence de chaque magasin
    path("api/getNbMagasinVente", views.ChaqueMagasin.as_view()),

    #retourne le nombre d'occurence de chaque Categorie
    path("api/getNbCategorieVente", views.ChaqueCategorie.as_view()),

    #retourne le nombre de produit qui apparait pas dans Vente
    path("api/compareVenteProduit",views.Compare.as_view()),

    #retourne la liste de tous les produits
    path("api/getAllProduitVente",views.GetAllProduit.as_view()),
    
    #entrée: un MAGID , sortie : une liste de produit ayant le MAGID
    path("api/getNbAllMagasin/<int:pk>", views.Magasin.as_view()),

    #entrée: un PRODID, sortie: une liste de produit ayant le PRODID
    path("api/getProduitVente/<int:pk>",views.GetProduit.as_view()),
    
    #entrée: DATE-MAGID / sortie : une liste de produit ayant la date et le MAGID
    path("api/getDate-Magasin/<str:date>/<int:pk>", views.DateMagasin.as_view()),

    #entrée: DATE-CATID / sortie : une liste de produit ayant la date et la CATID
    path("api/getDate-Categorie/<str:date>/<int:pk>", views.DateCategorie.as_view()),

    #entrée: DATEDEBUT-DATEFIN-MAGID / sortie : une liste de produit ayant MAGID entre 2 Dates
    path("api/getByDate/<str:debut>/<str:fin>/<int:magid>",views.ByDate.as_view()),

#######################################################NOUVELLES ROUTES ###################################################
    #retourne liste de tous les fabricants
    path("api/getAllFab",views.GetAllFab.as_view()),

    #entree: retourne le nombre de vente par magasin
    path("api/getMagidCount/", views.GetMagidCounts.as_view()),

    path("api/salesByMagasin/<int:pk>", views.SalesByMagasin.as_view()),
    # #entrée : fabId - sortie liste de des ventes par la fabricant
    # path("api/getSaleByFabId",views.getSaleByFabId.as_view()),

    #retourne top 10 des produits vendus produits
    path("api/GetTopNbVenteByProd/", views.GetTopNbVenteByProd.as_view()),

    #retourne une liste de produit par categorie pour un fabid
    path("api/getAllProductByCatAndByFabId/<int:pk>", views.getAllProductByCatAndByFabId.as_view()),

    #retourne une liste de produit par categorie pour un fabid
    path("api/getProdByIdByMonth/<int:pk>", views.getProdByIdByMonth.as_view()),

    #retourne une liste de produit par categorie pour un fabid
    path("api/getSaleByCatAndFabId/<int:pk>", views.getSaleByCatAndFabId.as_view()),

    #retourne une liste de produit par categorie pour un fabid
    path("api/top10ProdByCatAndFab/<int:pk>", views.top10ProdByCatAndFab.as_view()),

    #retourne une liste de produit par categorie pour un fabid
    path("api/top10Prod/", views.top10Prod.as_view()),

]
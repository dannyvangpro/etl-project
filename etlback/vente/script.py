import gzip,shutil
from django.conf import settings
import os 
from elasticsearch import Elasticsearch
import time
import json

from elasticsearch_dsl import Search, Q

#es = Elasticsearch( hosts=["https://localhost:9200"],http_auth=('myuser', 'password'),verify_certs=False)
index_name = "vente"

#Requete la liste des ventes avec une Date et un MAGID
def getDateMag(date,id):
    list = []
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
            "query": {
                "bool": {
                    "must": [
                        {"term": {"date": date}},
                        {"term": {"mag_id": id}}
                    ]
                }
            },
            "size": 1000  
        }
        )
    for e in val["hits"]["hits"] :
        list.append(e["_source"])
    return list
     
#Requete la liste des ventes avec une Date et un CATID
def getDateCat(date,id):
    list = []
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
            "query": {
                "bool": {
                    "must": [
                        {"term": {"date": date}},
                        {"term": {"cat_id": id}}
                    ]
                }
            },
            "size": 1000  
        }
        )
    for e in val["hits"]["hits"] :
        list.append(e["_source"])
    return list

#Requete qui retourne la liste ayant un MAGID
def getMagasin(mag):
    list=[]
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
                "query": {
                    "term": {
                        "mag_id": mag
                    }
                },
                "size": 10000 
            }
        )
    for e in val["hits"]["hits"] :
        list.append(e["_source"])
    return len(list)
    
    
#Requete la liste des ventes avec une Date et un MAGID
def eachMagasin(): 
    d = {}
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
                "size": 10000,
                "aggs": {
                    "fab_counts": {
                        "terms": {
                            "field": "mag_id",
                            "size": 4000000  # Mettez une valeur suffisamment élevée pour capturer tous les résultats
                        }
                    }
                }
            }
        )
    for e in val["hits"]["hits"] :
        key=e["_source"]["mag_id"]
        if key in d:
            d[key] += 1
        else:
           d[key] = 1
    
    return d

#Requete la liste des Categorie
def eachCategorie(): 
    d = {}
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
                "size": 10000,
                "aggs": {
                    "fab_counts": {
                        "terms": {
                            "field": "cat_id",
                            "size": 100000  # Mettez une valeur suffisamment élevée pour capturer tous les résultats
                        }
                    }
                }
            }
        )
    for e in val["hits"]["hits"] :
        key=e["_source"]["cat_id"]
        if key in d:
            d[key] += 1
        else:
           d[key] = 1
    
    return d

#Requete la liste des ventes entre la date d'intervalle debut-fin
def intervalleDate(debut,fin,magid): 
    d = {}
    compteur=0
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body = {
                    "size": 100,
                    "query": {
                        "bool": {
                            "filter": [
                                {
                                    "range": {
                                        "date": {
                                            "gte": debut,
                                            "lte": fin
                                        }
                                    }
                                },
                                {
                            "term": {
                                "mag_id": magid
                            }
                        }
                        ]
                        }
                    },
                    "aggs": {
                        "fab_counts": {
                            "terms": {
                                "field": "mag_id",
                                "size": 10000  # Mettez une valeur suffisamment élevée pour capturer tous les résultats
                            }
                        }
                    }
                }
        )
    for e in val["hits"]["hits"] :
        compteur+=1
        key=e["_source"]["cat_id"]
        if key in d:
            d[key] += 1
        else:
           d[key] = 1
        
    return d



def compareVenteProduit():
    start_time = time.time()

    # Récupérer tous les PRODID des deux index
    prodids_vente = fetch_all_prodids_opti('vente')
    prodids_produits = fetch_all_prodids_opti('produits')

    # Trouver les PRODID dans Produits qui ne sont pas dans Vente
    prodids_manquants = prodids_produits - prodids_vente

    end_time = time.time()

    elapsed_time = end_time - start_time

    print("PRODID dans 'Produits' qui ne sont pas dans 'Vente':", prodids_manquants)
    print(f"Temps d'exécution: {elapsed_time:.2f} secondes")
    return len(prodids_manquants)

def getProduit(prod):
    list=[]
    val= settings.ELASTICSEARCH_CLIENT.search(
            index=index_name,
            body={
                "query": {
                    "term": {
                        "prod_id": prod
                    }
                },
                "size": 10000 
            }
        )
    for e in val["hits"]["hits"] :
        list.append(e["_source"])
    return list

def fetch_all_prodids_opti(index_name):
    prodids = set()
    scroll_size = 10000
    body = {
        "size": scroll_size,
        "_source": False,  # Désactiver la récupération du champ _source
        "stored_fields": [],  # Nous ne voulons pas de champs stockés
        "docvalue_fields": ["prod_id"],  # Utiliser les doc values pour récupérer PRODID
        "query": {
            "match_all": {}
        }
    }

    response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=body, scroll='5m')  # Démarrer un scroll de 5 minutes
    while len(response['hits']['hits']):
        prodids.update([hit['fields']['prod_id'][0] for hit in response['hits']['hits']])
        scroll_id = response['_scroll_id']
        response = settings.ELASTICSEARCH_CLIENT.scroll(scroll_id=scroll_id, scroll='5m')

    return prodids

#compare les index ventes et produits
def fetch_all_prodids(index_name):
    prodids = set()
    scroll_size = 10000  # Taille du batch pour chaque requête
    body = {
        "size": scroll_size,
        "_source": ["prod_id"],
        "query": {
            "match_all": {}
        }
    }

    response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=body, scroll='5m')  # Démarrer un scroll de 5 minutes
    while len(response['hits']['hits']):
        prodids.update([hit['_source']['prod_id'] for hit in response['hits']['hits']])
        scroll_id = response['_scroll_id']
        response = settings.ELASTICSEARCH_CLIENT.scroll(scroll_id=scroll_id, scroll='5m')

    return prodids


def produits_par_mois():
    # Requête Elasticsearch avec une agrégation pour regrouper par mois
   body = {
        "size": 0,
        "aggs": {
            "products_per_month": {
                "date_histogram": {
                    "field": "date",
                    "calendar_interval": "month",
                    "format": "yyyy-MM"
                },
                "aggs": {
                    "product_count": {
                        "value_count": {
                            "field": "prod_id"
                        }
                    },
                }
            }
        }
    }
   response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=body)
   buckets = response["aggregations"]["products_per_month"]["buckets"]
   result = {entry["key_as_string"]: entry["product_count"]["value"] for entry in buckets}
   return result

def getAllFab():
    client = Elasticsearch([{'host': 'localhost', 'port': 9200}])  # Remplacez ces informations par les paramètres de votre cluster Elasticsearch

    s = Search(using=client, index=index_name)
    s = s.query(Q('term', cat_id=4))  # Filtre sur cat_id = 4, à adapter selon vos besoins
    s = s.source(['FABID'])  # Sélectionne uniquement la colonne FABID

    response = s.execute()

    fabids = [hit.FABID for hit in response]

    return fabids


def getMagidCounts():
    client = Elasticsearch([{'host': 'localhost', 'port': 9200}])  # Remplacez ces informations par les paramètres de votre cluster Elasticsearch

    s = Search(using=client, index=index_name)
    s = s.source([])  # N'inclut pas les sources (afin de ne pas récupérer les documents complets)
    
    # Agrégation de type "terms" sur le champ "MAGID" pour compter les occurrences
    s.aggs.bucket('magid_counts', 'terms', field='MAGID', size=1000)

    response = s.execute()

    # Extraire les résultats de l'agrégation
    magid_counts = [(bucket.key, bucket.doc_count) for bucket in response.aggregations.magid_counts.buckets]

    return magid_counts

def produits_par_mois_par_mag(mag_id):
    # Requête Elasticsearch avec une agrégation pour regrouper par mois et par MAGID
    body = {
        "size": 0,
        "query": {
            "term": {"mag_id": mag_id}  # Filtre par MAGID spécifié
        },
        "aggs": {
            "products_per_month": {
                "date_histogram": {
                    "field": "date",
                    "calendar_interval": "month",
                    "format": "yyyy-MM"
                },
                "aggs": {
                    "product_count": {
                        "value_count": {
                            "field": "prod_id"
                        }
                    }
                }
            }
        }
    }

    response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=body)
    buckets = response["aggregations"]["products_per_month"]["buckets"]
    result = {entry["key_as_string"]: entry["product_count"]["value"] for entry in buckets}
    return result


#retourne les 10 produits les plus vendus
def getTop10ProduitsLesPlusVendus():
   
    search_body = {
        "aggs": {
            "produits": {
                "terms": {
                    "field": "prod_id",
                    "size": 10  # Vous voulez les 10 produits les plus vendus
                }
            }
        }
    }

    response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=search_body)
    
    produits_agg = response["aggregations"]["produits"]["buckets"]
    
    return produits_agg


def ventes_par_categorie(fab_id):
    # Query Elasticsearch with an aggregation to group by category
    body = {
        "size": 0,
        "query": {
            "term": {"fab_id": fab_id}
        },
        "aggs": {
            "categories": {
                "terms": {
                    "field": "cat_id",
                    "size": 10  # Vous pouvez ajuster la taille en fonction de vos besoins
                },
                "aggs": {
                    "products": {
                        "top_hits": {
                            "size": 10,  # Le nombre maximum d'objets par catégorie
                            "_source": {
                                "includes": ["PRODID", "other_fields"]  # Inclure les champs nécessaires
                            }
                        }
                    }
                }
            }
        }
    }

    response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=body)
    buckets = response["aggregations"]["categories"]["buckets"]

    result = []
    for bucket in buckets:
        category = bucket["key"]
        products = bucket["products"]["hits"]["hits"]
        result.append({"category": category, "products": products})
    return result

def produits_par_mois_parIdProd(prod_id):
    # Requête Elasticsearch avec une agrégation pour regrouper par mois
    body = {
        "size": 0,
        "query": {
            "term": {"prod_id": prod_id}  # Filtre par prod_id spécifié
        },
        "aggs": {
            "products_per_month": {
                "date_histogram": {
                    "field": "date",
                    "calendar_interval": "month",
                    "format": "yyyy-MM"
                }
            }
        }
    }
    response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=body)
    buckets = response["aggregations"]["products_per_month"]["buckets"]
    result = {entry["key_as_string"]: entry["doc_count"] for entry in buckets}
    return result

def ventes_par_categorie_et_fabid(fab_id):
    # Requête Elasticsearch avec une agrégation pour regrouper par catégorie et par FABID
    body = {
        "size": 0,
        "query": {
            "term": {"fab_id": fab_id}  # Filtre par FABID spécifié
        },
        "aggs": {
            "categories": {
                "terms": {
                    "field": "cat_id",
                    "size": 10  # Vous pouvez ajuster la taille en fonction de vos besoins
                },
                "aggs": {
                    "product_count": {
                        "value_count": {
                            "field": "prod_id"
                        }
                    }
                }
            }
        }
    }

    response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=body)
    categories_buckets = response["aggregations"]["categories"]["buckets"]

    result = {}
    for category_bucket in categories_buckets:
        category = category_bucket["key"]
        product_count = category_bucket["product_count"]["value"]
        result[category] = product_count

    return result


def ventes_par_fabid(fab_id):
    # Requête Elasticsearch sans agrégation par catégorie
    body = {
        "size": 0,
        "query": {
            "term": {"fab_id": fab_id}  # Filtre par FABID spécifié
        },
        "aggs": {
            "product_count": {
                "terms": {
                    "field": "prod_id",
                    "size": 10  # Vous pouvez ajuster la taille en fonction de vos besoins
                }
            }
        }
    }

    response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=body)
    product_count_buckets = response["aggregations"]["product_count"]["buckets"]

    result = []

    for product_count_bucket in product_count_buckets:
        product_id = product_count_bucket["key"]
        count = product_count_bucket["doc_count"]
        result.append({product_id: count})

    return result

def top_10_produits_plus_vendus():
    body = {
        "size": 0,
        "aggs": {
            "produits": {
                "terms": {
                    "field": "prod_id",
                    "size": 10
                },
                "aggs": {
                    "ventes_total": {
                        "sum": {
                            "field": "quantite_vendue"
                        }
                    }
                }
            }
        }
    }

    response = settings.ELASTICSEARCH_CLIENT.search(index=index_name, body=body)
    produits_buckets = response["aggregations"]["produits"]["buckets"]

    top_10_produits = []

    for produit_bucket in produits_buckets:
        produit = produit_bucket["key"]
        quantite_ventes = produit_bucket["ventes_total"]["value"]

        top_10_produits.append({"prod_id": produit, "quantite_vendue": quantite_ventes})

    return top_10_produits
